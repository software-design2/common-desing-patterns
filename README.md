## Common Design Patterns

1. Singleton
2. Builder
3. Template Method
4. Factory Method
5. Abstract Factory
6. Facade
7. Adapter
8. Strategy
9. Observer
10. Decorator

based on https://refactoring.guru/