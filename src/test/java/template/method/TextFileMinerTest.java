package template.method;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TextFileMinerTest {

    @Test
    void shouldBehaveDifferentlyDependingOnTemplateImplementation() {
        //given
        List<String> dataReports = new ArrayList<>();
        TextFileMiner csvMiner = new CsvMiner();
        TextFileMiner docMiner = new DocMiner();

        //when
        dataReports.add(csvMiner.mine("file.csv"));
        dataReports.add(docMiner.mine("file.doc"));

        //then
        assertThat(dataReports).hasSize(2);
    }
}
