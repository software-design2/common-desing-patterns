package adapter;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SquarePegAdapterTest {

    @Test
    void shouldAdaptIncompatibleInterface() {
        //given
        RoundHole roundHole = new RoundHole(3);
        Round tooBigRoundPeg = new RoundPeg(4);
        SquarePeg fittingSquarePeg = new SquarePeg(4);
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(fittingSquarePeg);

        //when
        boolean roundPegPassed = roundHole.fits(tooBigRoundPeg);
        boolean squarePegPassed = roundHole.fits(squarePegAdapter);

        //then
        assertThat(roundPegPassed).isFalse();
        assertThat(squarePegPassed).isTrue();
    }
}
