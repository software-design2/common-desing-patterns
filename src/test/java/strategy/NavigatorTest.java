package strategy;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class NavigatorTest {

    @Test
    void shouldAllowSwappingStrategiesAtRuntimeWithinSingleContext() {
        //given
        RoutingStrategy fastestRouting = new FastestRouting();
        RoutingStrategy shortestRouting = new ShortestRouting();
        Navigator navigator = new Navigator(fastestRouting);

        //when
        Route fastestRoute = navigator.navigate();
        navigator.setRoutingStrategy(shortestRouting);
        Route shortestRoute = navigator.navigate();

        //then
        assertThat(fastestRoute.time()).isLessThanOrEqualTo(shortestRoute.time());
        assertThat(shortestRoute.distance()).isLessThanOrEqualTo(fastestRoute.distance());
    }
}
