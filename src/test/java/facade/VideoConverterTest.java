package facade;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

class VideoConverterTest {

    @Test
    void shouldUseFrameworkViaFacadeWithoutExposingDetails() {
        //given
        VideoConverter facade = new VideoConverter();

        //when
        File convertedFile = facade.convert("video.mp4", "video.wmv", VideoFormat.WMV);

        //then
        assertThat(convertedFile).isNotNull();
    }
}
