package observer;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StoreTest {

    @Test
    void shouldAddAndRemoveSubscribersDynamically() {
        //given
        SeasonalDiscountSubscriber subscriber = new SeasonalDiscountSubscriber();
        Publisher publisher = new Publisher();
        publisher.subscribe(EventType.SEASONAL_DISCOUNT, subscriber);
        Store store = new Store(publisher);

        //when
        store.startSeasonalDiscount(List.of("sunglasses"), "summer_beam");

        //then
        assertThat(subscriber.getMessage())
                .isEqualTo("Grab discount code: 'summer_beam' and save up to 1% on [sunglasses]!");
    }
}
