package factory.method.creator;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OperationTest {

    @Test
    void shouldBehaveDifferentlyDependingOnCreator() {
        //given
        double[] args = {2, 3, 5};
        Operation add = new AddOperation();
        Operation multiply = new MultiplyOperation();

        //when
        double sum = add.calculate(args);
        double product = multiply.calculate(args);

        //then
        assertThat(sum).isEqualTo(10);
        assertThat(product).isEqualTo(30);
    }
}
