package builder;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class HouseTest {

    @Test
    void shouldBuildObjectWithDefaultValues() {
        //when
        House house = House.builder()
                .windows(12)
                .floors(2)
                .withGarden()
                .build();

        //then
        assertThat(house.getEntrances()).isEqualTo(1);
        assertThat(house.getWindows()).isEqualTo(12);
        assertThat(house.getFloors()).isEqualTo(2);
        assertThat(house.hasGarden()).isTrue();
        assertThat(house.hasSwimmingPool()).isFalse();
    }
}
