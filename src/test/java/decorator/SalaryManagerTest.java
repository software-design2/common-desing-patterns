package decorator;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SalaryManagerTest {

    @Test
    void shouldAddBehaviorToObjectWithoutImpactOnClient() {
        //given
        DataSource payrollsSource = new FileSource();
        DataSource compressionDecorator = new CompressionDecorator(payrollsSource);
        DataSource encryptionDecorator = new EncryptionDecorator(compressionDecorator);
        SalaryManager salaryManager = new SalaryManager(encryptionDecorator);

        //when
        String payrollsData = salaryManager.getPayrolls("payrolls.txt");

        //then
        assertThat(payrollsData).isEqualTo("1000$");
    }
}
