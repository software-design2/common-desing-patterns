package abs.factory;

import abs.factory.web.WebButton;
import abs.factory.web.WebCheckbox;
import abs.factory.web.WebGuiFactory;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class GuiFactoryTest {

    @Test
    void shouldCreateProductsOfSameVariationWithoutRevealingSubclasses() {
        //given
        GuiFactory webGuiFactory = new WebGuiFactory();

        //when
        Button button = webGuiFactory.createButton();
        Checkbox checkbox = webGuiFactory.createCheckbox();

        //then
        assertThat(button).isInstanceOf(WebButton.class);
        assertThat(checkbox).isInstanceOf(WebCheckbox.class);
    }
}
