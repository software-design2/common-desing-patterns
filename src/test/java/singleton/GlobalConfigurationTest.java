package singleton;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class GlobalConfigurationTest {

    @Test
    void shouldAlwaysReturnTheSameInstance() {
        //when
        GlobalConfiguration obj1 = GlobalConfiguration.getInstance();
        GlobalConfiguration obj2 = GlobalConfiguration.getInstance();

        //then
        assertThat(obj1).isSameAs(obj2);
    }
}
