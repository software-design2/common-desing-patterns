package strategy;

//interface shared by all strategies
public interface RoutingStrategy {
    Route findRoute();
}
