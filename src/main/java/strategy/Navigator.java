package strategy;

/**
 * Behavioral design pattern:
 * - new strategies can be added without impact on the context (open-closed principle)
 * - possibility to swap algorithm at runtime
 * - relies on composition, not inheritance
 */
public class Navigator {
    private RoutingStrategy routingStrategy;

    public Navigator(RoutingStrategy routingStrategy) {
        this.routingStrategy = routingStrategy;
    }

    public void setRoutingStrategy(RoutingStrategy routingStrategy) {
        this.routingStrategy = routingStrategy;
    }

    public Route navigate() {
        return routingStrategy.findRoute();
    }
}
