package strategy;

public record Route(int distance, int time) {
}
