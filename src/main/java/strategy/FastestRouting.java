package strategy;

//concrete strategy
public class FastestRouting implements RoutingStrategy {
    @Override
    public Route findRoute() {
        return new Route(10, 1);
    }
}
