package strategy;

//concrete strategy
public class ShortestRouting implements RoutingStrategy {
    @Override
    public Route findRoute() {
        return new Route(1, 10);
    }
}
