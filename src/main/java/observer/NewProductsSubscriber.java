package observer;

//concrete subscriber
public class NewProductsSubscriber implements Subscriber {
    @Override
    public void notify(String message) {
        //react to notification
    }
}
