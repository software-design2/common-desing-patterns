package observer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Behavioral design pattern:
 * - establishes relations between objects at runtime
 * - possibility to add new subscribers without changing publisher (open-closed principle)
 * - subscribers don't know about existence of publisher
 */
public class Publisher {
    //event to subscribers map
    private final Map<EventType, SubscriptionList> eventToSubscriptionList = new HashMap<>();

    public boolean subscribe(EventType eventType, Subscriber subscriber) {
        if (!eventToSubscriptionList.containsKey(eventType)) {
            eventToSubscriptionList.put(eventType, new SubscriptionList());
        }
        SubscriptionList subscribers = eventToSubscriptionList.get(eventType);
        return subscribers.add(subscriber);
    }

    public boolean unsubscribe(EventType eventType, Subscriber subscriber) {
        if (!eventToSubscriptionList.containsKey(eventType)) {
            return false;
        }
        SubscriptionList subscribers = eventToSubscriptionList.get(eventType);
        return subscribers.remove(subscriber);
    }

    //notify all subscribers
    public void notify(EventType eventType, String message) {
        Optional.ofNullable(eventToSubscriptionList.get(eventType))
                .ifPresent(list -> list.getSubscribers().forEach(subscriber -> subscriber.notify(message)));
    }
}
