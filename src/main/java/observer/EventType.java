package observer;

public enum EventType {
    NEW_PRODUCTS, SEASONAL_DISCOUNT
}
