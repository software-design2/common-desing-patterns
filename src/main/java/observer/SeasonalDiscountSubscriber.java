package observer;

//concrete subscriber
public class SeasonalDiscountSubscriber implements Subscriber {
    private String message;

    @Override
    public void notify(String message) {
        //react to notification
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
