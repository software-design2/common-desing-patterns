package observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubscriptionList {
    private final List<Subscriber> subscribers = new ArrayList<>();

    public boolean add(Subscriber subscriber) {
        return subscribers.add(subscriber);
    }

    public boolean remove(Subscriber subscriber) {
        return subscribers.remove(subscriber);
    }

    public List<Subscriber> getSubscribers() {
        return Collections.unmodifiableList(subscribers);
    }
}
