package observer;

import java.util.List;

//contains business logic and uses publisher
public class Store {
    private final Publisher publisher;

    public Store(Publisher publisher) {
        this.publisher = publisher;
    }

    public void addNewProducts(List<String> products) {
        //add new products to store
        String message = String.format("Don't miss new products before they are sold out: %s!", products);
        publisher.notify(EventType.NEW_PRODUCTS, message);
    }

    public void startSeasonalDiscount(List<String> products, String discountCode) {
        //lower price of chosen products
        String message = String.format("Grab discount code: '%s' and save up to 1%% on %s!", discountCode, products);
        publisher.notify(EventType.SEASONAL_DISCOUNT, message);
    }
}
