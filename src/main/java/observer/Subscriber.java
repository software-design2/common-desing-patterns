package observer;

//subscriber interface
public interface Subscriber {
    void notify(String message);
}
