package builder;

/**
 * Creational design pattern:
 * - creates complex objects with optional/default members in a clean way
 * - different representations of same product without inheritance hierarchy or telescoping constructor
 */
public class House {
    private int entrances;
    private int windows;
    private int floors;
    private boolean garden;
    private boolean swimmingPool;

    public int getEntrances() {
        return entrances;
    }

    public int getWindows() {
        return windows;
    }

    public int getFloors() {
        return floors;
    }

    public boolean hasGarden() {
        return garden;
    }

    public boolean hasSwimmingPool() {
        return swimmingPool;
    }

    //private constructor and no setters to force builder usage
    private House() {
        //default member values
        entrances = 1;
        windows = 4;
        floors = 1;
    }

    //can be also defined as a public class in a separate file
    static class Builder {
        private final House house;

        public Builder() {
            house = new House();
        }

        //building methods return builder object itself to allow calls chaining
        public Builder entrances(int number) {
            /*instead of accessing members directly builder can duplicate fields
              and pass them to constructor in build() method*/
            house.entrances = number;
            return this;
        }

        public Builder windows(int number) {
            house.windows = number;
            return this;
        }

        public Builder floors(int number) {
            house.floors = number;
            return this;
        }

        public Builder withGarden() {
            house.garden = true;
            return this;
        }

        public Builder withSwimmingPool() {
            house.swimmingPool = true;
            return this;
        }

        //method to get built object
        public House build() {
            return house;
        }
    }

    //method to obtain builder
    public static Builder builder() {
        return new Builder();
    }
}
