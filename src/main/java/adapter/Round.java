package adapter;

//client interface
public interface Round {
    double getRadius();
}
