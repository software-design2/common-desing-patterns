package adapter;

//class which interface can't be changed (e.g. from 3rd party library)
public class SquarePeg {
    private final int width;

    public SquarePeg(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }
}
