package adapter;

//class which follows client interface
public class RoundPeg implements Round {
    private final int radius;

    public RoundPeg(int radius) {
        this.radius = radius;
    }

    @Override
    public double getRadius() {
        return radius;
    }
}
