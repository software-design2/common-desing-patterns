package adapter;

/**
 * Structural design pattern:
 * - separates interface or data conversion from business logic (single responsibility)
 * - allows to add more adapters without breaking the client (open-closed principle)
 * - if possible it's often simpler to update interface of incompatible class instead
 */
public class SquarePegAdapter implements Round {
    //wrap class with incompatible interface
    private final SquarePeg squarePeg;

    public SquarePegAdapter(SquarePeg squarePeg) {
        this.squarePeg = squarePeg;
    }

    //convert interface or format of data to interface used by client
    @Override
    public double getRadius() {
        //radius of the smallest circle that can accommodate the square peg
        return squarePeg.getWidth() * Math.sqrt(2) / 2;
    }
}
