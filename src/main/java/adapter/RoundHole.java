package adapter;

//client
public class RoundHole {
    private final int radius;

    public RoundHole(int radius) {
        this.radius = radius;
    }

    public boolean fits(Round shape) {
        return radius >= shape.getRadius();
    }
}
