package abs.factory.system;

import abs.factory.Checkbox;

//concrete product implementation (variation compliant)
public class SystemCheckbox implements Checkbox {
    @Override
    public void tick() {
    }
}
