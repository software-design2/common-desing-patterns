package abs.factory.system;

import abs.factory.Button;
import abs.factory.Checkbox;
import abs.factory.GuiFactory;

//creates specific variation of all products
public class SystemGuiFactory implements GuiFactory {
    @Override
    public Button createButton() {
        return new SystemButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new SystemCheckbox();
    }
}
