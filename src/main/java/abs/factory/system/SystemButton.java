package abs.factory.system;

import abs.factory.Button;

//concrete product implementation (variation compliant)
public class SystemButton implements Button {
    @Override
    public void click() {
    }
}
