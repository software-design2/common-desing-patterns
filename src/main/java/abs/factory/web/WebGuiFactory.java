package abs.factory.web;

import abs.factory.Button;
import abs.factory.Checkbox;
import abs.factory.GuiFactory;

//creates specific variation of all products
public class WebGuiFactory implements GuiFactory {
    @Override
    public Button createButton() {
        return new WebButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WebCheckbox();
    }
}
