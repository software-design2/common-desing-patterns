package abs.factory.web;

import abs.factory.Button;

//concrete product implementation (variation compliant)
public class WebButton implements Button {
    @Override
    public void click() {
    }
}
