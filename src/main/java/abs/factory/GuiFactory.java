package abs.factory;

/**
 * Creational design pattern:
 * - low coupling between client and concrete products
 * - promotes open-closed principle (just add new variation of products)
 * - ensures products belonging to single variation are compatible with each other
 */
public interface GuiFactory {
    //creates all product types belonging to family
    Button createButton();

    Checkbox createCheckbox();
}
