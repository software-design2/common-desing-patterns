package template.method;

import java.io.File;

/**
 * Behavioral design pattern:
 * - defines common algorithm flow and eliminates duplication
 * - promotes open-closed principle (just add new algorithm variation)
 * - be careful to not violate Liskov substitution principle (while overriding default implementation from superclass)
 */
public abstract class TextFileMiner {

    //template method (final so subclasses can override only particular steps of the algorithm)
    public final String mine(String filePath) {
        File file = openFile(filePath);
        String rawData = parseData(file);
        String data = analyzeData(rawData);
        return generateReport(data);
    }

    //common implementation
    protected File openFile(String filePath) {
        return new File(filePath);
    }

    protected String generateReport(String data) {
        return "";
    }

    //subclass specific implementation
    protected abstract String parseData(File file);

    protected abstract String analyzeData(String rawData);
}
