package singleton;

/**
 * Creational design pattern:
 * - ensures class has single, shared instance
 * - provides global access point to the instance
 */
public class GlobalConfiguration {
    //private static instance
    private static GlobalConfiguration instance;

    //private constructor
    private GlobalConfiguration() {
    }

    //public static instance creation method
    public static GlobalConfiguration getInstance() {
        //lazy initialization
        if (instance == null) {
            instance = new GlobalConfiguration();
        }
        return instance;
    }

    public void doSomething() {
        //some business logic
    }
}
