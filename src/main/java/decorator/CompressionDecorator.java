package decorator;

//concrete decorator
public class CompressionDecorator extends DataSourceDecorator {

    public CompressionDecorator(DataSource dataSource) {
        //pass wrapee to base decorator
        super(dataSource);
    }

    @Override
    public String readData(String path) {
        //go down behavior stack
        String data = super.readData(path);
        //add new behavior
        return decompress(data);
    }

    private String decompress(String data) {
        //fake decompression logic for the brevity of example
        return data.replace(".compressed", "");
    }

    @Override
    public void writeData(String path, String content) {
        String compressedData = compress(content);
        super.writeData(path, compressedData);
    }

    private String compress(String content) {
        //fake compression logic for the brevity of example
        return content + ".compressed";
    }
}
