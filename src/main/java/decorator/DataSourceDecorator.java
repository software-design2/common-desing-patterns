package decorator;

/**
 * Structural design pattern:
 * - adds/removes behaviors to/from object at runtime without impact on client
 * - allows to create all combinations of defined behaviors
 * - splits behavior variants into separate classes (single responsibility principle)
 */
public abstract class DataSourceDecorator implements DataSource {
    //wrapee implementing common interface
    private final DataSource dataSource;

    public DataSourceDecorator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String readData(String path) {
        return dataSource.readData(path);
    }

    @Override
    public void writeData(String path, String content) {
        dataSource.writeData(path, content);
    }
}
