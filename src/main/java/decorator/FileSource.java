package decorator;

//concrete interface implementation
public class FileSource implements DataSource {
    @Override
    public String readData(String path) {
        //read data from given path
        return "1000$.compressed.encrypted";
    }

    @Override
    public void writeData(String path, String content) {
        //save data under given path
    }
}
