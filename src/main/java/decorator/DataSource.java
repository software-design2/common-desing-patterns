package decorator;

//common interface
public interface DataSource {
    String readData(String path);

    void writeData(String path, String content);
}
