package decorator;

//client relying on common interface
public class SalaryManager {
    private final DataSource dataSource;

    public SalaryManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getPayrolls(String path) {
        return dataSource.readData(path);
    }

    public void updatePayrolls(String path, String payrolls) {
        dataSource.writeData(path, payrolls);
    }
}
