package decorator;

//concrete decorator
public class EncryptionDecorator extends DataSourceDecorator {
    public EncryptionDecorator(DataSource dataSource) {
        //pass wrapee to base decorator
        super(dataSource);
    }

    @Override
    public String readData(String path) {
        //go down behavior stack
        String data = super.readData(path);
        //add new behavior
        return decrypt(data);
    }

    private String decrypt(String data) {
        //fake decryption logic for the brevity of example
        return data.replace(".encrypted", "");
    }

    @Override
    public void writeData(String path, String content) {
        String encryptedData = encrypt(content);
        super.writeData(path, encryptedData);
    }

    private String encrypt(String content) {
        //fake encryption logic for the brevity of example
        return content + ".encrypted";
    }
}
