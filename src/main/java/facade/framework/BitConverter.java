package facade.framework;

import facade.framework.codec.Codec;

import java.io.File;
import java.io.InputStream;

//Complicated code of 3rd party framework
public class BitConverter {
    public InputStream read(VideoFile input, Codec codec) {
        return null;
    }

    public File convert(InputStream content, String outputPath) {
        return new File(outputPath);
    }
}
