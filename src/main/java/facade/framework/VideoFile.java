package facade.framework;

import java.io.File;

public class VideoFile extends File {
    public VideoFile(String filePath) {
        super(filePath);
    }
}
