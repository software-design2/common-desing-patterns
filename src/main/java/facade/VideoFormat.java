package facade;

public enum VideoFormat {
    MPEG4, WMV
}
