package facade;

import facade.framework.BitConverter;
import facade.framework.VideoFile;
import facade.framework.codec.Codec;
import facade.framework.codec.Mpeg4CompressionCodec;
import facade.framework.codec.WmvCompressionCodec;

import java.io.File;
import java.io.InputStream;

/**
 * Structural design pattern:
 * - provides simplified interface to complex (often 3rd party) subsystem
 * - makes rest of the system framework-agnostic (when framework is changed then only facade needs to be updated)
 */
public class VideoConverter {
    private final BitConverter converter;

    public VideoConverter() {
        converter = new BitConverter();
    }

    //simplified interface to complex subsystem
    public File convert(String inputPath, String outputPath, VideoFormat outputFormat) {
        VideoFile sourceFile = new VideoFile(inputPath);
        Codec codec = getFormatCodec(outputFormat);
        InputStream input = converter.read(sourceFile, codec);
        return converter.convert(input, outputPath);
    }

    private Codec getFormatCodec(VideoFormat format) {
        return switch (format) {
            case WMV -> new WmvCompressionCodec();
            case MPEG4 -> new Mpeg4CompressionCodec();
        };
    }
}
