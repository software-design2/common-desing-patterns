package factory.method.product;

//concrete product implementation
public class Addition implements Calculable {
    @Override
    public double calculate(double... args) {
        int sum = 0;
        for (double arg : args) {
            sum += arg;
        }
        return sum;
    }
}
