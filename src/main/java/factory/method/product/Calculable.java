package factory.method.product;

//common product interface
public interface Calculable {
    double calculate(double... args);
}
