package factory.method.product;

//concrete product implementation
public class Multiplication implements Calculable {
    @Override
    public double calculate(double... args) {
        double product = 1;
        for (double arg : args) {
            product *= arg;
        }
        return product;
    }
}
