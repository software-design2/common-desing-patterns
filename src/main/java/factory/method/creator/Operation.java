package factory.method.creator;

import factory.method.product.Calculable;

/**
 * Creational design pattern:
 * - low coupling between creator and concrete products
 * - supports open-closed principle (easy to add new product type)
 */
public abstract class Operation {

    //implements flow and business logic, creation via factory method is less important
    public double calculate(double... args) {
        Calculable calculable = createOperation();
        double result = calculable.calculate(args);
        trackOperationHistory(args, result);
        return result;
    }

    public void trackOperationHistory(double[] args, double result) {
        //business logic
    }

    //abstract factory method
    abstract protected Calculable createOperation();
}
