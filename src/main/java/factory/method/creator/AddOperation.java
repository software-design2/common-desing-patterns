package factory.method.creator;

import factory.method.product.Addition;
import factory.method.product.Calculable;

//concrete creator implementation
public class AddOperation extends Operation {
    @Override
    protected Calculable createOperation() {
        return new Addition();
    }
}
