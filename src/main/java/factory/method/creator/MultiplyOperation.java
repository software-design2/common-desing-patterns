package factory.method.creator;

import factory.method.product.Calculable;
import factory.method.product.Multiplication;

//concrete creator implementation
public class MultiplyOperation extends Operation {
    @Override
    protected Calculable createOperation() {
        return new Multiplication();
    }
}
